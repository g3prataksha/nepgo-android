package com.example.harjit.nepgo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class register extends Activity implements View.OnClickListener {

    private static EditText editFirstname, editLastname, editCity;
    private static EditText editEmail, editPassword, editConfirmpassword;
    private static Button button_login, button_signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        editFirstname = (EditText) findViewById(R.id.editTextFirstName);
        editLastname = (EditText) findViewById(R.id.editTextLastName);
        editCity = (EditText) findViewById(R.id.editTextCity);
        editEmail = (EditText) findViewById(R.id.editTextEmail);
        editPassword = (EditText) findViewById(R.id.editTextPassword);
        editConfirmpassword = (EditText) findViewById(R.id.editTextConfirmPassword);

        button_login = (Button) findViewById(R.id.button_login);
        button_login.setOnClickListener(this);
        button_signup = (Button) findViewById(R.id.button_signup);
        button_signup.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == findViewById(R.id.button_login)) {
            Intent intent = new Intent(register.this, MainActivity.class);
            startActivity(intent);
            this.finish();
        } else if (v == findViewById(R.id.button_signup)) {
            String firstname = editFirstname.getText().toString();
            String lastname = editLastname.getText().toString();
            String city = editCity.getText().toString();
            String email = editEmail.getText().toString();
            String password = editPassword.getText().toString();
            String confirmpassword = editConfirmpassword.getText().toString();

            inputValidationHelper validate = new inputValidationHelper();
            if (firstname.isEmpty() || lastname.isEmpty() ||
                    city.isEmpty() || email.isEmpty() ||
                    password.isEmpty() || confirmpassword.isEmpty()) {
                Toast.makeText(getApplicationContext(),
                        "Please fill in all the details",
                        Toast.LENGTH_SHORT).show();
            } else if (!validate.isValidEmail(email)) {
                Toast.makeText(getApplicationContext(),
                        "Please type a valid email address",
                        Toast.LENGTH_SHORT).show();
            } else if (!validate.isValidPassword(password, true)) {
                Toast.makeText(getApplicationContext(),
                        "Password not valid",
                        Toast.LENGTH_SHORT).show();
            } else if (!password.equals(confirmpassword)) {
                Toast.makeText(getApplicationContext(),
                        "Password do not match",
                        Toast.LENGTH_SHORT).show();
            } else {
                User user = new User();
                user.firstname = firstname;
                user.lastname = lastname;
                user.city = city;
                user.email = email;
                user.password = password;

                DBManager mydb = new DBManager(this);
                mydb.insert(user);

                Toast.makeText(getApplicationContext(),
                        "Signed up",
                        Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(this, MainActivity.class);
                finish();
            }

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
