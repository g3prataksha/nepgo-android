package com.example.harjit.nepgo;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MessageDescription extends Fragment {
    TextView sender, title, description;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_message_description, container, false);
    }

    @Override
    public void onViewCreated(View view,
                              Bundle savedInstanceState) {
        Bundle b = getArguments();
        if (b != null) {
            String sendername = b.getString("sendername");
            String msg_title = b.getString("title");
            String receivername = b.getString("receivername");
            getMessageDescription(sendername, receivername, msg_title);

        } else {
            Log.v("no", "bundle");
        }
    }

    public void getMessageDescription(String sendername,
                                      String receivername,
                                      String msg_title){
        DBManager mydb = new DBManager(getActivity());
        Cursor message = mydb.getSpecificMessage(sendername, receivername, msg_title);
        TextView sender = (TextView) getView().findViewById(R.id.text_sendername);
        TextView title = (TextView) getView().findViewById(R.id.text_message_title);
        TextView description = (TextView) getView().findViewById(R.id.text_message_description);

        sender.setText("Sent by: " + message.getString(message.getColumnIndexOrThrow(Message.KEY_SENDER)));
        title.setText(message.getString(message.getColumnIndexOrThrow(Message.KEY_TITLE)));
        description.setText(message.getString(message.getColumnIndexOrThrow(Message.KEY_DESCRIPTION)));
    }
}
