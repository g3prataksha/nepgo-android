package com.example.harjit.nepgo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListActivity;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Layout;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.ArrayList;
import java.util.HashMap;


public class timeline extends ListActivity implements View.OnClickListener {
    private FragmentTransaction fragmentTransaction;
    Button btnlogout, btnview_users, btnown_timeline, btnInbox;
    TextView user_first_name, user_last_name;
    TextView firstname, lastname;
    private String firstName, lastName;
    Button btnMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        Intent intent = getIntent();                    //getting intent from MainActivity with email passed as value
        String email = intent.getStringExtra("email");

        DBManager mydb = new DBManager(this);
        Cursor user = mydb.getUserByEmail(email);       //gets the user from database using email id
        firstName = user.getString(user.getColumnIndexOrThrow(User.KEY_FIRST_NAME));
        lastName = user.getString(user.getColumnIndexOrThrow(User.KEY_LAST_NAME));

        TextView user_first_name = (TextView) findViewById(R.id.user_first_name);
        TextView user_last_name = (TextView) findViewById(R.id.user_last_name);
        user_first_name.setText("Profile of " + firstName);
        user_last_name.setText(lastName);

        btnown_timeline = (Button) findViewById(R.id.buttonTimeline);
        btnlogout = (Button) findViewById(R.id.buttonLogout);
        btnview_users = (Button) findViewById(R.id.buttonUsers);
        btnMsg = (Button) findViewById(R.id.button_message);
        btnInbox = (Button) findViewById(R.id.buttonInbox);

        btnown_timeline.setVisibility(View.INVISIBLE);
        btnown_timeline.setOnClickListener(this);
        btnlogout.setOnClickListener(this);
        btnview_users.setOnClickListener(this);
        btnInbox.setOnClickListener(this);
        btnview_users.performClick();
    }

    @Override
    public void onClick(View view) {
        if (view == findViewById(R.id.buttonUsers)) {   //When the other users button is clicked it gets all the users from database
            user_first_name = (TextView) findViewById(R.id.user_first_name);
            user_last_name = (TextView) findViewById(R.id.user_last_name);

            DBManager mydb = new DBManager(this);

            ArrayList<HashMap<String, String>> userList = mydb.getUserList(
                    user_first_name.getText().toString(),
                    user_last_name.getText().toString()
            );

            if (userList.size() != 0) {
                final ListView lv = getListView();
                lv.setVisibility(View.VISIBLE);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {      //This sets the list of users from database and add it to the list view
                        firstname = (TextView) view.findViewById(R.id.firstname);
                        lastname = (TextView) view.findViewById(R.id.lastname);
                        String userFirstName = firstname.getText().toString();
                        String userLastName = lastname.getText().toString();

                        lv.setVisibility(View.INVISIBLE);

                        ProfileFragment pf = new ProfileFragment();     //setting up a new fragment when clicked on a specific user in the list
                        Bundle bundle = new Bundle();
                        bundle.putString("firstname", userFirstName);
                        bundle.putString("lastname", userLastName);
                        pf.setArguments(bundle);                        //providing values like firstname and lastname as arguments
                        ShowProfileFragment(pf);

                        btnMsg.setVisibility(View.VISIBLE);
                        user_first_name.setVisibility(View.INVISIBLE);
                        user_last_name.setVisibility(View.INVISIBLE);
                    }
                });
                ListAdapter adapter = new SimpleAdapter(
                        timeline.this,
                        userList,
                        R.layout.users,
                        new String[]{"firstname", "lastname"},
                        new int[]{R.id.firstname, R.id.lastname});

                setListAdapter(adapter);
                btnown_timeline = (Button) findViewById(R.id.buttonTimeline);
                btnown_timeline.setVisibility(View.VISIBLE);
                btnview_users.setVisibility(View.INVISIBLE);

                user_first_name = (TextView) findViewById(R.id.user_first_name);
                user_last_name = (TextView) findViewById(R.id.user_last_name);
                user_first_name.setText(firstName);
                user_first_name.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                user_last_name.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            } else {
                Toast.makeText(this, "No Users!", Toast.LENGTH_SHORT).show();   //If database is empty
            }
        } else if (view == findViewById(R.id.buttonLogout)) {                   //When logout button is pressed
            AlertDialog.Builder logout_alert = new AlertDialog.Builder(this);
            logout_alert.setMessage("Are you sure you want to Logout?");
            logout_alert.setCancelable(true);

            logout_alert.setPositiveButton(
                    "Logout",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                            timeline.this.finish();
                        }
                    });
            logout_alert.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    }
            );

            AlertDialog alertDialog = logout_alert.create();
            alertDialog.show();

        } else if (view == findViewById(R.id.buttonTimeline)) {             //Button for going back to the main profile or timeline
            ListView lv = getListView();
            lv.setVisibility(View.INVISIBLE);

            android.app.Fragment profile = new android.app.Fragment();
            fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.ProfileFragment, profile);
            fragmentTransaction.commit();

            btnMsg.setVisibility(View.INVISIBLE);
            btnown_timeline.setVisibility(View.INVISIBLE);
            btnview_users.setVisibility(View.VISIBLE);
            user_first_name.setVisibility(View.VISIBLE);
            user_last_name.setVisibility(View.VISIBLE);
            user_first_name.setText("Profile of " + firstName);
            user_first_name.setTextSize(35);
            user_last_name.setTextSize(35);
        } else if (view == findViewById(R.id.buttonInbox)) {

            Intent intent = new Intent(timeline.this, inbox.class);
            String receiver = firstName;
            Log.v("receiver", receiver);
            intent.putExtra("receiver", receiver);
            startActivity(intent);
        }
    }

    public void ShowProfileFragment(ProfileFragment pf) {
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ProfileFragment, pf);
        fragmentTransaction.commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
