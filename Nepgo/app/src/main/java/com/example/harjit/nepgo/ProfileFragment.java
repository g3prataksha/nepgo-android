package com.example.harjit.nepgo;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ProfileFragment extends Fragment implements View.OnClickListener{
    View ProfileView;
    private FragmentTransaction fragmentTransaction;
    private String firstName, lastName;
    TextView sendername, receivername;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ProfileView = inflater.inflate(R.layout.fragment_profile, container, false);

        Button btn_send_msg = (Button) ProfileView.findViewById(R.id.button_message);
        btn_send_msg.setOnClickListener(this);

        return ProfileView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
//        Getting the values passed to the fragment from timeline activity
        Bundle b = getArguments();
        if (b != null) {
            firstName = b.getString("firstname");
            lastName = b.getString("lastname");
            setText(firstName, lastName);
        } else {
            Log.v("bundle :", "null");
        }

    }

    public void onClick(View v) {
        if (v == ProfileView.findViewById(R.id.button_message)) {
            sendername = (TextView) getActivity().findViewById(R.id.user_first_name);
            String senderName = sendername.getText().toString();

            Fragment fm = getFragmentManager().findFragmentById(R.id.ProfileFragment);
            receivername= (TextView) fm.getView().findViewById(R.id.fragment_firstname);
            String receiverName = receivername.getText().toString();

            Intent intent = new Intent(getActivity(), WriteMessage.class);
            intent.putExtra("sendername", senderName);
            intent.putExtra("receivername", receiverName);
            startActivity(intent);
        }
    }

    //Sets the values provided from timeline to the textview in Profile Fragment
    public void setText(String firstName, String lastName) {
        TextView fragment_firstname = (TextView) getView().findViewById(R.id.fragment_firstname);
        TextView fragment_lastname = (TextView) getView().findViewById(R.id.fragment_lastname);

        fragment_firstname.setText(firstName);
        fragment_lastname.setText(lastName);

        fragment_firstname.measure(0, 0);
        int width = fragment_firstname.getMeasuredWidth();
        fragment_lastname.setPadding(width + 15, 0, 0, 0);
    }

}
