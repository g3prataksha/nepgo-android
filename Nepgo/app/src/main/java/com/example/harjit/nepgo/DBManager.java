package com.example.harjit.nepgo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Harjit on 1/12/2017.
 */
public class DBManager {
    private DBHelper dbHelper;
    private MessageDBHelper mdbHelper;

    public DBManager(Context context) {
        dbHelper = new DBHelper(context);
        mdbHelper = new MessageDBHelper(context);
    }

    //Inserting User into Database
    public int insert(User user) {
        //open connection for writing data
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(User.KEY_FIRST_NAME, user.firstname);
        values.put(User.KEY_LAST_NAME, user.lastname);
        values.put(User.KRY_CITY, user.city);
        values.put(User.KEY_EMAIL, user.email);
        values.put(User.KEY_PASSWORD, user.password);

        //insert row
        long user_id = db.insert(User.TABLE, null, values);
        db.close();
        return (int) user_id;
    }

    //Deleting User from Database
//    public void delete(int user_id) {
//        SQLiteDatabase db = dbHelper.getWritableDatabase();
//        db.delete(User.TABLE, User.KEY_NAME + "=?", new String[]{String.valueOf(user_id)});
//        db.close();
//    }

    //Updating User
    public void update(User user) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(User.KEY_FIRST_NAME, user.firstname);
        values.put(User.KEY_LAST_NAME, user.lastname);
        values.put(User.KRY_CITY, user.city);
        values.put(User.KEY_EMAIL, user.email);
        values.put(User.KEY_PASSWORD, user.password);

        db.update(User.TABLE, values, User.KEY_ID + "=?", new String[]{String.valueOf(user.user_id)});
    }

    //Getting List of Other users for messaging and others
    public ArrayList<HashMap<String, String>> getUserList(String firstname, String lastname) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT  " +
                User.KEY_FIRST_NAME + ", " +
                User.KEY_LAST_NAME + ", " +
                User.KRY_CITY + ", " +
                User.KEY_EMAIL + " FROM " +
                User.TABLE + " WHERE " +
                User.KEY_FIRST_NAME + "!='" + firstname + "' AND " +
                User.KEY_LAST_NAME + "!='" + lastname + "';";

        ArrayList<HashMap<String, String>> userList = new ArrayList<HashMap<String, String>>();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            for (int i = 0; i < cursor.getCount(); i++) {
                HashMap<String, String> user = new HashMap<String, String>();
                user.put("firstname", cursor.getString(cursor.getColumnIndex(User.KEY_FIRST_NAME)));
                user.put("lastname", cursor.getString(cursor.getColumnIndex(User.KEY_LAST_NAME)));
                userList.add(user);
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();
        return userList;
    }

    //Getting User with name as input
    public Cursor getUserByEmail(String mail) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM " +
                User.TABLE + " WHERE " +
                User.KEY_EMAIL + "='" + mail + "'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    //Getting User with name as input
    public Cursor getUserByName(String name) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM " +
                User.TABLE + " WHERE " +
                User.KEY_FIRST_NAME + "='" + name + "'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    //Checking if the username and password matches
    public boolean validateUser(String mail, String pass) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT " + User.KEY_EMAIL + ", " +
                User.KEY_PASSWORD + " FROM " +
                User.TABLE + " WHERE " +
                User.KEY_EMAIL + "='" + mail + "' AND " +
                User.KEY_PASSWORD + "='" + pass + "'";

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0)
            return true;
        return false;
    }

    //Checking if user exists
    public boolean checkUser(String firstname, String lastname) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT " + User.KEY_FIRST_NAME + ", " +
                User.KEY_LAST_NAME + " FROM " +
                User.TABLE + " WHERE " +
                User.KEY_FIRST_NAME + "='" + firstname + "' AND " +
                User.KEY_LAST_NAME + "='" + lastname + "'";

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.getCount() > 0)
            return true;
        return false;
    }

    //Insert Messages
    public int insertMessage(Message message) {
        SQLiteDatabase db = mdbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Message.KEY_SENDER, message.sender);
        values.put(Message.KEY_RECEIVER, message.receiver);
        values.put(Message.KEY_TITLE, message.title);
        values.put(Message.KEY_DESCRIPTION, message.description);

        //insert row
        long message_id = db.insert(Message.TABLE, null, values);
        db.close();
        return (int) message_id;
    }

    //Getting List of Messages in Inbox
    public ArrayList<HashMap<String, String>> getInboxMessages(String receiver) {
        SQLiteDatabase db = mdbHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM " +
                Message.TABLE + " WHERE " +
                Message.KEY_RECEIVER + "='" + receiver + "'";

        ArrayList<HashMap<String, String>> messages = new ArrayList<HashMap<String, String>>();

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            for (int i = 0; i < cursor.getCount(); i++) {
                HashMap<String, String> message = new HashMap<String, String>();
                message.put("sender", cursor.getString(cursor.getColumnIndex(Message.KEY_SENDER)));
                message.put("receiver", cursor.getString(cursor.getColumnIndex(Message.KEY_RECEIVER)));
                message.put("title", cursor.getString(cursor.getColumnIndex(Message.KEY_TITLE)));
                messages.add(message);
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();
        return messages;
    }

    //Getting specific message
    public Cursor getSpecificMessage(String sendername,
                                     String receivername,
                                     String title) {
        SQLiteDatabase db = mdbHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM " +
                Message.TABLE + " WHERE " +
                Message.KEY_SENDER + "='" + sendername + "' AND " +
                Message.KEY_RECEIVER + "='" + receivername + "' AND " +
                Message.KEY_TITLE + "='" + title + "'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }
}
