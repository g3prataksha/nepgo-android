package com.example.harjit.nepgo;

/**
 * Created by Harjit on 2/2/2017.
 */
public class Message {
    public static final String TABLE = "Message";

    public static final String KEY_ID = "id";
    public static final String KEY_SENDER = "sender";
    public static final String KEY_RECEIVER = "receiver";
    public static final String KEY_TITLE = "title";
    public static final String KEY_DESCRIPTION = "description";

    public int message_id;
    public String sender, receiver, title, description;
}
