package com.example.harjit.nepgo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Harjit on 2/2/2017.
 */
public class MessageDBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 4;
    private static final String DATABASE_NAME = "message";
    public MessageDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_MESSAGE = "CREATE TABLE " + Message.TABLE + "("
                + "ID INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + Message.KEY_SENDER + " TEXT, "
                + Message.KEY_RECEIVER + " TEXT, "
                + Message.KEY_TITLE + " TEXT, "
                + Message.KEY_DESCRIPTION + " TEXT )";
        db.execSQL(CREATE_TABLE_MESSAGE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Message.TABLE);
        onCreate(db);
    }
}
