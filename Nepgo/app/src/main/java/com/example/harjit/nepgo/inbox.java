package com.example.harjit.nepgo;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;


public class inbox extends ListActivity implements View.OnClickListener{
    FragmentTransaction ft;
    TextView sendername, title;
    Button timeline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);

        Intent intent = getIntent();
        final String receivername = intent.getStringExtra("receiver");

        DBManager mydb = new DBManager(this);
        ArrayList<HashMap<String, String>> messageList = mydb.getInboxMessages(receivername);
        if (messageList.size() != 0) {
            final ListView listView = getListView();
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    sendername = (TextView) view.findViewById(R.id.sender);
                    title = (TextView) view.findViewById(R.id.title);

                    MessageDescription messageDescription = new MessageDescription();
                    Bundle bundle = new Bundle();
                    bundle.putString("sendername", sendername.getText().toString());
                    bundle.putString("title", title.getText().toString());
                    bundle.putString("receivername", receivername);

                    messageDescription.setArguments(bundle);

                    ft = getFragmentManager().beginTransaction();
                    ft.replace(R.id.MessageDescription, messageDescription).commit();

                    listView.setVisibility(View.INVISIBLE);
                    TextView inboxinfo = (TextView) findViewById(R.id.textInboxpage);
                    inboxinfo.setVisibility(View.INVISIBLE);
                }
            });

            ListAdapter adapter = new SimpleAdapter(
                    inbox.this,
                    messageList,
                    R.layout.messages,
                    new String[]{"sender", "title"},
                    new int[]{R.id.sender, R.id.title});
            setListAdapter(adapter);
        } else {
            Toast.makeText(this, "No messages", Toast.LENGTH_SHORT).show();
        }

        timeline = (Button) findViewById(R.id.buttonTimeline);
        timeline.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_inbox, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
    if (v == findViewById(R.id.buttonTimeline)){
        Intent intent = new Intent(getApplicationContext(), timeline.getClass());
        finish();
    }
    }
}
