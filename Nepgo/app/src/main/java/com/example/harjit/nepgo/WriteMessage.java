package com.example.harjit.nepgo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class WriteMessage extends Activity implements View.OnClickListener{
    private String senderName, receiverName;
    Button btnSendMsg;
    EditText editTitle, editDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_message);

        Intent intent = getIntent();
        senderName = intent.getStringExtra("sendername");
        receiverName = intent.getStringExtra("receivername");

        btnSendMsg = (Button) findViewById(R.id.button_send_msg);
        btnSendMsg.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == findViewById(R.id.button_send_msg)) {
            editTitle = (EditText) findViewById(R.id.edit_message_title);
            editDescription = (EditText) findViewById(R.id.edit_message_description);

            Message message = new Message();
            message.sender = senderName;
            message.receiver = receiverName;
            message.title = editTitle.getText().toString();
            message.description = editDescription.getText().toString();
            if (checkIfnull(message)){
                DBManager mydb = new DBManager(this);
                mydb.insertMessage(message);
                Toast.makeText(getApplicationContext(),
                        "Sent",
                        Toast.LENGTH_SHORT).show();
            }
            Intent intent = new Intent(this, timeline.class);
            finish();
        }
    }

    public boolean checkIfnull(Message message) {
        if (message.sender.isEmpty()) {
            Toast.makeText(getApplicationContext(), "No sender name", Toast.LENGTH_SHORT).show();
        } else if (message.receiver.isEmpty()) {
            Toast.makeText(getApplicationContext(), "No receiver name", Toast.LENGTH_SHORT).show();
        } else if (message.title.isEmpty()) {
            Toast.makeText(getApplicationContext(), "No title", Toast.LENGTH_SHORT).show();
        } else if (message.description.isEmpty()) {
            Toast.makeText(getApplicationContext(), "No description", Toast.LENGTH_SHORT).show();
        } else {
            return true;
        }
        return false;
    }
}
