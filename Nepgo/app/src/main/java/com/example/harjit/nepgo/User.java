package com.example.harjit.nepgo;

/**
 * Created by Harjit on 1/12/2017.
 */
public class User {
    public static final String TABLE = "User";

    public static final String KEY_ID = "id";
    public static final String KEY_FIRST_NAME = "firstname";
    public static final String KEY_LAST_NAME = "lastname";
    public static final String KRY_CITY = "city";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PASSWORD = "password";

    public int user_id;
    public String firstname, lastname, city, email;
    public String password;

}
