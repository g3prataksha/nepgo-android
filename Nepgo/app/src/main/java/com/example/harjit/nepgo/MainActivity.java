package com.example.harjit.nepgo;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;


public class MainActivity extends FragmentActivity implements View.OnClickListener {

    private static EditText email;
    private static EditText password;
    private static Button login_button, register_button;

    private CallbackManager callbackManager;
    private LoginButton fb_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_main);

        login_button = (Button) findViewById(R.id.button_login);
        register_button = (Button) findViewById(R.id.button_reg);
        email = (EditText) findViewById(R.id.editText);
        password = (EditText) findViewById(R.id.editText2);

        fb_login = (LoginButton) findViewById(R.id.button_facebook_login);

        fb_login.setOnClickListener(this);
        login_button.setOnClickListener(this);
        register_button.setOnClickListener(this);
        login_button.performClick();
    }

    @Override
    public void onClick(View v) {
        //If Login button is pressed
        if (v == findViewById(R.id.button_login)) {
            String mail = "harry@kane.com";//email.getText().toString();
            String pass = "harrykane";//password.getText().toString();

            DBManager mydb = new DBManager(this);

            inputValidationHelper validate = new inputValidationHelper();

            if (validate.isNullOrEmpty(mail) || validate.isNullOrEmpty(pass)) {
                Toast.makeText(getApplicationContext(), "Please insert email and password",
                        Toast.LENGTH_SHORT).show();
            } else if (!validate.isValidEmail(mail)) {
                Toast.makeText(getApplicationContext(), "Email is not correct format",
                        Toast.LENGTH_SHORT).show();
            } else if (mydb.validateUser(mail, pass)) { //Checks if the username and password match in the database
                Toast.makeText(getApplicationContext(), "Welcome",
                        Toast.LENGTH_SHORT).show();

                //Gets the user from the database
                Cursor user = mydb.getUserByEmail(mail);
                String email = user.getString(user.getColumnIndexOrThrow(User.KEY_EMAIL));
                Intent intent = new Intent(getApplicationContext(), timeline.class);
                intent.putExtra("email", email);
                startActivity(intent);
            } else {
                //If the username and password do not match
                Toast.makeText(getApplicationContext(), "Username and password does not match",
                        Toast.LENGTH_SHORT).show();
            }

            //If register button is pressed
        } else if (v == findViewById(R.id.button_reg)) {
            Intent intent = new Intent(MainActivity.this, register.class);
            startActivity(intent);

        } else if (v == findViewById(R.id.button_facebook_login)) {
            fb_login.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

                private ProfileTracker mProfileTracker;

                @Override
                public void onSuccess(LoginResult loginResult) {
                    if (Profile.getCurrentProfile() == null) {
                        mProfileTracker = new ProfileTracker() {
                            @Override
                            protected void onCurrentProfileChanged(Profile profile, Profile profile2) {
                                // profile2 is the new profile

                                String name = profile2.getName();
                                Log.v("facebook - profile", name);

                                DBManager mydb = new DBManager(getApplicationContext());
                                if (mydb.checkUser(profile2.getFirstName(), profile2.getLastName())) {
                                    Toast.makeText(getApplicationContext(),
                                            name + " logged in",
                                            Toast.LENGTH_SHORT).show();
//                                    Intent intent = new Intent(MainActivity.this, timeline.class);
//                                    intent.putExtra("name", name);
//                                    startActivity(intent);
                                } else {
                                    User user = new User();
                                    user.firstname = profile2.getFirstName();
                                    user.lastname = profile2.getLastName();

                                    mydb.insert(user);
                                    Toast.makeText(getApplicationContext(),
                                            name,
                                            Toast.LENGTH_SHORT).show();
                                }

                                mProfileTracker.stopTracking();
                            }
                        };
                        // no need to call startTracking() on mProfileTracker
                        // because it is called by its constructor, internally.
                    } else {
                        Profile profile = Profile.getCurrentProfile();
                        Log.v("facebook - profile", profile.getFirstName());
//                        Intent intent = new Intent(MainActivity.this, timeline.class);
//                        intent.putExtra("name", name);
//                        startActivity(intent);
                    }
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException e) {

                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
