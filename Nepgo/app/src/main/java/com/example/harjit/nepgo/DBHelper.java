package com.example.harjit.nepgo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Harjit on 1/12/2017.
 */
public class DBHelper extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION = 4;
    private static final String DATABASE_NAME = "user";
    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_USER = "CREATE TABLE " + User.TABLE + "("
                + "ID INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + User.KEY_FIRST_NAME + " TEXT, "
                + User.KEY_LAST_NAME + " TEXT, "
                + User.KRY_CITY + " TEXT, "
                + User.KEY_EMAIL + " TEXT, "
                + User.KEY_PASSWORD + " TEXT)";
        db.execSQL(CREATE_TABLE_USER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + User.TABLE);
        onCreate(db);
    }
}
